﻿[APPLICATION]
OpenIdConnectAuthorizationCodeFlow


[VERSION]
v00.00.01

[DEPENDENCIES]
- Markarian.MachineConfig v01.06.00
- Markarian.Tools.XmlConfigurator v01.07.00
- IIS 7 met DotNetCore.1.0.4_1.1.1-WindowsHosting

[DESCRIPTION OF THE CHANGES]
v00.00.01
- Alpha version.


[INSTALLATION GUIDE INITIAL]
- Create an AD service account for the website's application pool.
- Set the application pool's .NET Framework version to "No Managed Code".


[INSTALLATION GUIDE]
- Follow the instructions in the installation wizard.
- The installation procedure may not yield any errors.




[CONFIGURATION]
- Start the XmlConfigurator.
- Verify settings below 'Web Applications\OpenIdConnectAuthorizationCodeFlow' and adjust as needed.


[KNOWN ISSUES]
