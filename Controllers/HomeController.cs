﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OpenIdConnectAuthorizationCodeFlow.Controllers {
  [Authorize]
  public class HomeController: Controller {
    // GET: /<controller>/
    public IActionResult Index() {
      ViewBag.AccessToken = HttpContext.Session.GetString("access_token");
      return Redirect($"https://jwt.io/?value={ViewBag.AccessToken}");
    }
  }
}
