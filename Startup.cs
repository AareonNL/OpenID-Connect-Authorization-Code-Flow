﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace OpenIdConnectAuthorizationCodeFlow {
  public class Startup {
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="env"></param>
    public Startup(IHostingEnvironment env) {
      Configuration = new ConfigurationBuilder()
        .SetBasePath(System.AppContext.BaseDirectory)
        .AddXmlFile("OAuth.xml")
        .Build();
    }

    private Task OnAuthenticationFailed(FailureContext context) {
      context.HandleResponse();
      context.Response.Redirect("/?error=" + context.Failure.Message);
      return Task.FromResult(0);
    }

    private Task OnTokenResponseReceived(TokenResponseReceivedContext context) {
      context.HttpContext.Session.SetString("access_token", context.TokenEndpointResponse.AccessToken);
      return Task.FromResult(0);
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services) {
      services.AddMvc();
      services.AddSession();
      services.AddAuthentication(sharedOptions => sharedOptions.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
      loggerFactory.AddConsole();

      if(env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
      }

      app.UseSession();

      app.UseCookieAuthentication(new CookieAuthenticationOptions());

      app.UseOpenIdConnectAuthentication(new OpenIdConnectOptions {
        MetadataAddress = Configuration["metadata"],
        ClientId = Configuration["client:id"],
        ClientSecret = Configuration["client:secret"],
        ResponseType = OpenIdConnectResponseType.Code,
        GetClaimsFromUserInfoEndpoint = false,

        Events = new OpenIdConnectEvents {
          OnRemoteFailure = OnAuthenticationFailed,
          OnTokenResponseReceived = OnTokenResponseReceived
        }
      });

      app.UseMvc(routes => {
        routes.MapRoute(
            name: "default",
            template: "{controller=Home}/{action=Index}/{id?}");
      });
    }

    public IConfigurationRoot Configuration { get; }
  }
}
