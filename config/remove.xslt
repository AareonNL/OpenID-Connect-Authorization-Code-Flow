﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xc="urn:XmlConfiguration"
>

  <!-- START CONFIGURATION -->

  <xsl:variable name="file_name">..\WebApps\$[application]\$[application].xml</xsl:variable>

  <!-- END CONFIGURATION -->


  <xsl:output method="xml" indent="yes" />


  <!-- ROOT NODE -->
  <xsl:template match="/">
    <xsl:if test="not(/*/xc:section[@name='Web Applications'])">
      <xsl:message terminate="yes">incorrect template</xsl:message>
    </xsl:if>
    <xsl:if test="not(/*/xc:section[@name='Web Applications']/xc:file[@name=$file_name])">
      <xsl:message terminate="yes">
        <xsl:text>file </xsl:text>
        <xsl:value-of select="$file_name"/>
        <xsl:text> not present</xsl:text>
      </xsl:message>
    </xsl:if>
    <xsl:if test="not(//xc:section)">
      <xsl:message terminate="yes">couldn't find xml-configurator section</xsl:message>
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>


  <xsl:template match="/*/xc:section[@name='Web Applications']/xc:file">
    <xsl:choose>
      <xsl:when test="@name=$file_name"/>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- COPY RECURSIVELY -->
  <xsl:template match="@* | node()">
    <xsl:choose>
      <xsl:when test="./*">
        <xsl:copy>
          <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
